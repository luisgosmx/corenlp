## Setup

get in screen
`screen -ls` show available

enter `screen -r` + name of screen

put the next command

`java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9010 -timeout 15000`

exit `control a+d`
